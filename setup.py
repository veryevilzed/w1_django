# -*- coding: utf-8 -*-
from setuptools import setup

setup(
    name = "w1_django",
    version = "0.0.1",
    description = "Модуль кошелька W1 для django",
    long_description = open('README.md').read(),
    packages = ['w1'],
    zip_safe = False,
    install_requires = ['django<1.5'],
)
