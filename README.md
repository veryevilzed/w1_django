Модуль кошелька W1 для django
=============================

Для установки при помощи pip добавить в файл зависимостей:

    git+https://bitbucket.org/naoai/w1_django.git

###В корне проекта в файле settings.py

Указать константы в словаре W1_CONFIG:

    W1_COMFIG = {
        'W1_SECRET_KEY'   : цифровая подпись кошелька, @type str
        'W1_MERCHANT_ID'  : id кошелька, @type int
        'W1_SUCCESS_URL'  : ссылка при удачном завершении, @type str
        'W1_FAIL_URL'     : ссылка при неудачном завершении, @type str
        'W1_PAID_URL'     : ссылка для редиректа, если заказ был уже оплачен, @type str
        'W1_FORM_TPL'     : шаблон формы для передачи оплаты на w1.ru, @type str
        'W1_USER_MODEL'   : Модель пользователя, @type django.models
    }

Подключить модуль:

    INSTALLED_APPS = (..., 'w1', ...)

###В urls.py корня проекта добавить:

    urlpatterns = patterns(..., url(r'^url_модуля/', include('w1.urls')), ...)

----------

В настройках кошелька в личном кабинете указать:

* Тип цифровой подписи: __SHA1__
* Адрес для оповещений: http://example.com/url_модуля/process/

----------

[Репозиторий](https://bitbucket.org/ookami_kb/w1/) основного приложения кошелька W1.