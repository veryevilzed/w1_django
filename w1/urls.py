from django.conf.urls.defaults import patterns, url
from w1.views import payment_form, process

urlpatterns = patterns('',
     url(r'^(?P<oid>\d+)/$', payment_form),
     url(r'^process/$', process),
)
