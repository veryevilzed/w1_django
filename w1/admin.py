from django.contrib import admin
from w1.models import Order

    
class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'created', 'paid', 'amount')
    readonly_fields = ('purse', 'order_id')
    
admin.site.register(Order, OrderAdmin)