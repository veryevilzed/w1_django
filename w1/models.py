# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import models
import datetime
from w1.signals import transaction_paid

class Order(models.Model):
    user = models.ForeignKey(settings.W1_CONFIG['W1_USER_MODEL'], verbose_name=u'Плательщик')
    created = models.DateTimeField(u'Создана', auto_now_add=True)
    paid = models.DateTimeField(u'Оплачена', blank=True, null=True)
    purse = models.CharField(u'Кошелек', max_length=12, blank=True, null=True)
    order_id = models.CharField(u'№ заказа', max_length=12, blank=True, null=True)
    amount = models.FloatField(u'Сумма')
    
    def pay(self, purse, order_id, amount):
        self.purse = purse
        self.order_id = order_id
        self.paid = datetime.datetime.now()
        self.amount = amount
        self.save()
        transaction_paid.send(sender=self)
        
    def is_paid(self):
        return self.paid is not None