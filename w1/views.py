# -*- coding: utf-8 -*-
from django.conf import settings
# from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import HttpResponse, get_object_or_404, render_to_response
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_exempt
from w1.forms import W1Form
from w1.models import Order
import base64
import hashlib

# @login_required
def payment_form(request, oid):
    order = get_object_or_404(Order, pk=oid)
    if order.is_paid():
        # Заказ уже оплачен
        return HttpResponseRedirect(settings.W1_CONFIG['W1_PAID_URL'])
    initial = {
               'WMI_PAYMENT_AMOUNT': order.amount,
               'WMI_PAYMENT_NO': order.id,
               'WMI_DESCRIPTION': 'Payment'
               }
    form = W1Form(initial=initial)
    form.set_signature()
    return render_to_response(settings.W1_CONFIG['W1_FORM_TPL'],
                              {'form': form},
                              RequestContext(request))

@csrf_exempt    
def process(request):
    try:
        signature = request.POST.get('WMI_SIGNATURE', '')
        
        secret_key = settings.W1_CONFIG['W1_SECRET_KEY']
        
        data = request.POST.copy()
        del data['WMI_SIGNATURE']
        data = data.items()
        data.sort(key=lambda x: x[0].lower())
        
        params = [value for _, value in data]
        
        s = ''.join(params) + secret_key
        key = base64.b64encode(hashlib.sha1(s).digest())
        if key != signature:
            return HttpResponse('WMI_RESULT=OK')
        
        payment_no = request.POST.get('WMI_PAYMENT_NO', '')
        try:
            order = Order.objects.get(pk=payment_no)
        except Order.DoesNotExist:
            return HttpResponse('WMI_RESULT=OK')
        
        state = request.POST.get('WMI_ORDER_STATE', '')
        if state == 'Accepted':
            order.pay(
                purse=request.POST.get('WMI_TO_USER_ID', 'NO PURSE'),
                order_id=request.POST.get('WMI_ORDER_ID', 0),
                amount=request.POST.get('WMI_PAYMENT_AMOUNT',0)
            )
    except Exception as e:
        print e    
    return HttpResponse('WMI_RESULT=OK')
